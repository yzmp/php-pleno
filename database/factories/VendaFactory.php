<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Venda;
use Faker\Generator as Faker;

$factory->define(Venda::class, function (Faker $faker) {
    return [
        'valor_da_venda' => $faker->randomFloat(2, 1, 100000),
        /* 'vendedor_id' => function () {
            return factory(App\Vendedor::class)->create()->id;
        }, */
    ];
});
