<?php

use Illuminate\Database\Seeder;

class VendedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* App\Vendedor::create([
            'name' => str_random(10),
            'email' => str_random(10) . '@gmail.com',
        ]); */

        factory(App\Vendedor::class, 10)->create()->each(function ($vendedor) {
            $vendedor->vendas()->createMany(
                factory(App\Venda::class, rand(1, 7))->make()->toArray()
            );
        });
    }
}
