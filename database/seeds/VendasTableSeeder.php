<?php

use Illuminate\Database\Seeder;

class VendasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* App\Venda::create([
            'valor_da_venda' => rand(1, 100000 * 100) / 100,
            'vendedor_id' => 1,
        ]); */

        factory(App\Venda::class, 50)->create()->each(function ($venda) {
            $venda->vendedores()->save(factory(App\Vendedor::class)->make());
        });
    }
}
