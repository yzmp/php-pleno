<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vendedores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'email'];

    /**
     * Validation rules for the attributes.
     *
     * @var array
     */
    public $rules = [
        'nome' => 'required|max:60',
        'email' => 'required|email|max:60',
    ];

    /**
     * Each Vendedor may have multiple Vendas.
     *
     * @return
     */
    public function vendas() {
        return $this->hasMany('App\Venda');
    }

    /**
     * Vendedor's total comissao.
     *
     * @return float
     */
    public function getComissaoAttribute() {
        return floatval($this->vendas()->sum('comissao'));
    }
}
