<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['valor_da_venda', 'vendedor_id'];

    /**
     * Validation rules for the attributes.
     *
     * @var array
     */
    public $rules = [
        'valor_da_venda' => 'required|numeric',
        'vendedor_id' => 'required|exists:vendedores,id',
    ];

    /**
     * Each Venda belongs to one Vendedor.
     *
     * @return
     */
    public function vendedor() {
        return $this->belongsTo('App\Vendedor');
    }

    /**
     * Sets comissao to 6.5% of valor_da_venda.
     *
     * @param  string  $value
     * @return void
     */
    protected function setValorDaVendaAttribute($value) {
        $this->attributes['valor_da_venda'] = $value;
        $this->attributes['comissao'] = round($value * 65 / 1000, 2);
    }

    /**
     * Gets data_da_venda attribute.
     *
     * @return
     */
    public function getDataDaVendaAttribute() {
        return $this->created_at->toDateTimeString();
    }
}
