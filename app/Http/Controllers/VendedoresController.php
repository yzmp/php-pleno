<?php

namespace App\Http\Controllers;

use App\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

class VendedoresController extends Controller
{
    const CACHE_EXPIRATION = 60;

    /**
     * Lists all vendedores.
     * 
     * @return
     */
    public function index() {
        $vendedores = Cache::remember('vendedores', self::CACHE_EXPIRATION, function () {
            return Vendedor::all();
        });

        return response()->json($vendedores->map(function ($vendedor, $key) {
            return [
                'id' => $vendedor->id,
                'nome' => $vendedor->nome,
                'email' => $vendedor->email,
                'comissao' => $vendedor->comissao,
            ];
        }));
    }

    /**
     * Shows details for one vendedor.
     * 
     * @param  int  $id
     * @return
     */
    public function show($id) {
        $vendedor = Cache::remember('vendedor-' . intval($id), self::CACHE_EXPIRATION, function () use ($id) {
            $vendedor = Vendedor::find($id);

            return [
                'id' => $vendedor->id,
                'nome' => $vendedor->nome,
                'email' => $vendedor->email,
                'vendas' => $vendedor->vendas->map(function ($venda, $key) {
                    return [
                        'valor_da_venda' => $venda->valor_da_venda,
                        'comissao' => $venda->comissao,
                        'data_da_venda' => $venda->data_da_venda,
                    ];
                }),
            ];
        });

        if(!$vendedor) {
            return response()->json([
                'message'   => 'Not found',
            ], 404);
        }

        return response()->json($vendedor);
    }

    /**
     * Creates a vendedor.
     * 
     * @param  Request  $request
     * @return
     */
    public function store(Request $request) {
        $vendedor = new Vendedor();
        $data = $request->all();

        $validator = Validator::make($data, $vendedor->rules);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()
            ], 422);
        }

        $vendedor->fill($data);
        $vendedor->save();

        Cache::forget('vendedores');

        return response()->json([
            'id' => $vendedor->id,
            'nome' => $vendedor->nome,
            'email' => $vendedor->email,
        ], 201);
    }
}
