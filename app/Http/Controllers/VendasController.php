<?php

namespace App\Http\Controllers;

use App\Venda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

class VendasController extends Controller
{
    /**
     * Creates a venda.
     * 
     * @param  Request  $request
     * @return
     */
    public function store(Request $request) {
        $venda = new Venda();
        $data = $request->all();

        $validator = Validator::make($data, $venda->rules);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()
            ], 422);
        }

        $venda->fill($data);
        $venda->save();

        Cache::forget('vendedor-' . intval($venda->vendedor_id));

        return response()->json([
            'id' => $venda->vendedor_id,
            'nome' => $venda->vendedor->nome,
            'email' => $venda->vendedor->email,
            'valor_da_venda' => $venda->valor_da_venda,
            'comissao' => $venda->comissao,
            'data_da_venda' => $venda->data_da_venda,
        ], 201);
    }
}
