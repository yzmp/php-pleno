<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * Tests root redirection to api.
     *
     * @return void
     */
    public function testRootRedirection()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    /**
     * Tests if listing Vendedores is working.
     *
     * @return void
     */
    public function testListVendedores()
    {
        $response = $this->get('/api/vendedores');

        $response->assertStatus(200);
    }

    /**
     * Tests the creation of a vendedor.
     *
     * @return void
     */
    public function testCreateVendedor()
    {
        $nome = 'John Doe';
        $email = 'johndoe@example.com';

        $response = $this->json('POST', '/api/vendedores', [
            'nome' => $nome,
            'email' => $email,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'nome',
                'email',
            ]);

        $this->assertDatabaseHas('vendedores', [
            'nome' => $nome,
            'email' => $email,
        ]);
    }

    /**
     * Tests the creation of a venda.
     *
     * @return void
     */
    public function testCreateVenda()
    {
        $vendedor_id = 1;
        $valor_da_venda = 100;

        $response = $this->json('POST', '/api/vendas', [
            'vendedor_id' => $vendedor_id,
            'valor_da_venda' => $valor_da_venda,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'nome',
                'email',
                'valor_da_venda',
                'comissao',
                'data_da_venda',
            ]);

        $this->assertDatabaseHas('vendas', [
            'vendedor_id' => $vendedor_id,
            'valor_da_venda' => $valor_da_venda,
            'comissao' => $valor_da_venda * 65 / 1000,
        ]);
    }

    /**
     * Tests if listing Vendedor is working.
     *
     * @return void
     */
    public function testListVendedor()
    {
        $response = $this->get('/api/vendedores/1');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'nome',
                'email',
                'vendas',
            ]);
    }
}
